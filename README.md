_this is custom, project specific (and somewhat ugly) code that should not be used for any other purposes than analyzing the eyeIR experiment -- Wouter_

# Parsing data from LTpriming - natural scenes 
The critical file is `write_eyeIR.py` and `process_hdf5.py`.
The first writes the data to an (pytables) hdf-5 table; 
the second reads this table and selects samples from trials.

# Folder hierarchy:
The code assumes that the following folders are in the same directory as the source files

### ascdata/
Where the parser will put the ascii--output of EDF2ASC 

### EDF2ASC_WIN32/
Contains edf2asc.exe and edfapi.dll  ; will run with `WINE`
If your system has edf2asc installed to run from the command line, this folder isn't necessary, but you will have to modify the code in the EDF2ASC class in `eyedata_parser.py`
### edfdata/
Should contain the `*.edf` datafiles; Eyelink data files

# Get started
Use the custom `write_eyeIR.py`. This is an executable standalone program that is used for two separate tasks:

 1. it runs edf2asc to produce ascii files  from the edf-files, with sample- and event- data;
 2. it parses its output under 1. into an hdf5-table (using pytables)

Its CLI arguments are as follows

```bash
usage: write_ltpnat.py [-h] [-E] [-i [INPATH]] [-o [OUTPATH]]
                       [-f FILENAME [FILENAME ...]] [-t [TABNAME]]
                       [-b [BEHAVIORAL_PATH]] [-B]

optional arguments:
  -h, --help            show this help message and exit
  -E, --EDF2ASC         Run EDF2ASC if -E is given, runs parser otherwise
  -i [INPATH], --inpath [INPATH]
                        inpath, of the edfs (with -E) or of the evt/gaz files
  -o [OUTPATH], --outpath [OUTPATH]
                        (only for -E) where to put resulting asc-data
  -f FILENAME [FILENAME ...], --filename FILENAME [FILENAME ...]
                        file(s) to process with the parser
  -t [TABNAME], --tabname [TABNAME]
                        name of the table to save
  -b [BEHAVIORAL_PATH], --behavioral_path [BEHAVIORAL_PATH]
                        Path to behavioral expt output files
  -B, --batch_run       (with -E) run EDF2ASC on all files in folder
```
(`-b` is used for something not used here -- which is process 'behavioral' csv output with an R script, and store the result in the h5 table as well. This is not used here.)

I'm well aware this CLI is really, really awkward as of yet, but one way to use it (i.e. translate ALL edf files to asc, 
then store them all into a table 'eyeIR.h5' would be.

```bash
./write_ltpnat.py -E -B -i edfdata -o ascdata
./write_ltpnat.py -f `basename -a ascdata/*.gaz` -t 'eyeIR.h5'
```

# Further notes:
As the data come in, you could run edf2asc on single edf files by omitting `-B` and using `-f` to indicate the single file instead. 
You could use the same command to write the data to the table though, because the writer **will skip groups (i.e. participants/datasets) that are already in the data**. 

Then proceed by running code like that in `process_hdf5.py`, for further processing needs.
You can browse hdf5 tables with a tool like `vitables`. This also allows you to manually modify tables or query them.




