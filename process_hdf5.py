import pandas as pd
import numpy as np

# get the data
with pd.get_store('eyeIR.h5') as store:
    table=store['eyeIR001/segment_data']
    gzdata=store['eyeIR001/gaze_segments']

# markers that 'mark' start end end of requested segment:
marker = ['start_trial', 'stop_trial']
epoch = np.array([0, 0])

# get the timepoints of those markers:
tm = np.array(table[marker[0]+'_tstamp'])[:,np.newaxis]
tm2 = np.array(table[marker[1]+'_tstamp'])[:,np.newaxis]
tm = np.hstack((tm,tm2))
tranges = tm + np.array(epoch)

# subset the table:
t = np.array(gzdata.time)
f =  ( (tranges[:,0,np.newaxis] < t[np.newaxis,:]) * 
        (tranges[:,1,np.newaxis] > t[np.newaxis,:]) ).any(axis=0)
gzdata[f]
