import os, re
import numpy as np
from itertools import izip

import rpy2.robjects as R
import pandas.rpy.common  as com
from StringIO import StringIO

from IPython import embed
e2a_cmd = 'wine EDF2ASC_WIN32/edf2asc.exe '

"""
Maybe construct a config - yaml/json file?
To tell us:
 - the edf data folder
 - the asc data folder
 - the R-script (and that an R-script is used)
  - rather than relying on cmdline args 
"""

class ProcessBehavioralOutput(object):
    """docstring for ProcessBehavioralOutput"""
    def __init__(self, ipath='.'):
        super(ProcessBehavioralOutput, self).__init__()
        self.ipath = ipath
        self.behavioral_data = None

    def process_behav_file(self, *args,**kwargs):
        """Define how to process the behavioral expt file output
        For me,. this means run an R-script that produces the desired data
        """
        self.R_hook(*args,**kwargs)
        return
    
    def R_hook(self, data_fname, script_name=None, RFun='rpy_hook'):
        """
        Preprocessing: runs an R-script that deals with behavioral data
        """
        if os.path.dirname(data_fname) == '':
            data_fname = os.path.join(self.beh_path,data_fname )
        stem = os.path.splitext(data_fname)[0]
        if not data_fname.endswith('.csv'): # assume only stem used in call
            data_fname += '.csv'

        if script_name is None: # assume [expt][xx][aa] naming for data_fname
            expt = re.findall(
                re.compile('([^0-9]+)\d+'), os.path.basename(data_fname))
            if expt == []:
                script_name = 'preproc_expt.r'
            else:
                script_name = 'preproc_expt_{}.r'.format(expt[0])
        ## interfacing with R:
        try:
            with open(script_name) as sf:
                script = sf.read()
            r_result = R.r(script)
            rpyhook = R.r[RFun]
            # set filename (globally) in environment:
            R.r("fname <<- '{}'".format(data_fname))
            # run the stuff:
            r_result = rpyhook()
            # clear R workspace (i.e. fname and functions)
            R.r('rm(list=ls())')
        except IOError, e:
            print "Script [{0}] not found; skipped".format(script_name)
            raise e
        except ValueError, e:
            print "Error in script".format(script_name)
            raise e
        except Exception, e: # is lookuperror -- no idea where it is defined
            print "Function {} not found".format(RFun)
            raise e

        # turn it into a pandas dataframe.
        self.behavioral_df = com.convert_robj(r_result)
        # or turn into list of dicts:
        self.behavioral_data = [ i[1].to_dict() 
          for i in self.behavioral_df.iterrows() ]
        return

"""
EDF2ASC calls the windows-executable EDF2ASC with the appropriate arguments;
Its is designed for batch processing; so construct one EDF2ASC object to 
deal with many files at the same time. This is a lengthy process that 
you'll only have to do once.
"""
class EDF2ASC(object):
    """EDF2ASC"""
    def __init__(self, inpath='.',outpath='.'):
        super(EDF2ASC, self).__init__()
        self.ipath = inpath
        self.opath = outpath

    def batch_run(self):
        # get all edf files in inpath:
        edfs = [f for f in os.listdir(self.ipath) 
                        if os.path.splitext(f)[1]=='.edf']
        print("found {} files...".format(len(edfs)) )
        for f in edfs: self.create_asc(f)
        return

    def create_asc(self, fname):
        # run wine
        cmd = e2a_cmd
        if not fname.startswith(self.ipath):
            iname = os.path.join(self.ipath, fname)
        stem = os.path.splitext(fname)[0] 
        gz_oname =  os.path.join(self.opath, stem) + '.gaz'
        ev_oname =  os.path.join(self.opath, stem) + '.evt'
        ascfile =   os.path.join(self.opath, stem) + '.asc'
        self.gzcmd = (
            '{} -y -z -v -s -miss 0.0001 -vel -p {} {}'.format(
                cmd, self.opath, iname) +
            '; mv {} {}'.format(ascfile, gz_oname) )

        self.evcmd = (
            '{} -y -z -v -e -miss 0.0001 -vel -p {} {}'.format(
                cmd, self.opath, iname) +
            '; mv {} {}'.format(ascfile, ev_oname) )

        print("Executing cmd {}".format(self.gzcmd))
        os.system(self.gzcmd)
        print("Executing cmd {}".format(self.evcmd))
        os.system(self.evcmd)
        # clean gaze data, and turn to np array
        self.gzclean(gz_oname)
        self.gznp(gz_oname)
        return
    
    def gzclean(self,fname,write=False):
        with open(fname) as gf:
            self.gstr = gf.read()
        self.gstr = re.sub(re.compile('[A-Z]+'), '', self.gstr)
        self.gstr = re.sub(re.compile('\t+\.+'), '', self.gstr)
        return
    
    def gznp(self,fname):
        print("Saving gaze data")
        dummyfile = StringIO(self.gstr)
        gzdatnp = np.genfromtxt(dummyfile)
        gzdatnp[gzdatnp==0.0001] = np.nan # nans are nans
        np.save(fname, gzdatnp)
        return
            
# what do data columns in gaze-data mean? 
_dcol_meanings = dict(
    LR = ['time','L_gaze_x','L_gaze_y','L_pupil',
                 'R_gaze_x','R_gaze_y','R_pupil',
                 'L_vel_x','L_vel_y','R_vel_x','R_vel_y'],
    R = ['time','R_gaze_x','R_gaze_y','R_pupil','R_vel_x','R_vel_y'],
    L = ['time','L_gaze_x','L_gaze_y','L_pupil','L_vel_x','L_vel_y'],
)
class ASCParser(object):
    """
    Processes the output of EDF2ASC; 
    """
    def __init__(self, infile, behavioral_path=None):
        super(ASCParser, self).__init__()
        # - get stem of infile
        self.evt_str = None
        self.stem, ext = os.path.splitext(infile)
        self.inpath, self.stem = os.path.split(self.stem)
        if not self.inpath: self.inpath='.' 
        assert ext in ['.gaz','.evt','.npy','']
        self.load_event_data()
        self.gzdat = None

        # re's found in the event file; with interesting
        self._regex = dict(
            # fetch sample rate and recorded eye
            srate_eye = 'MSG\s[\d\.]+\s!MODE RECORD CR (\d+) \d+ \d+ (\S+)',
            # fetch screen_resolution:
            resolution = ('MSG\s[\d\.]+\sGAZE_COORDS ' + 
                         '(\d+.\d+)\s(\d+.\d+)\s(\d+.\d+)\s(\d+.\d+)'),
            # end of saccs,fix's blinks, list all relevant properties
            # Saccades, fixations and blinks:
            sacc = 'ESACC.+',
            fix = "EFIX.+",
            blink = "EBLINK.+"
        )
        self.beh_path = behavioral_path
        return

    def load_event_data(self):
        if self.evt_str:
            return

        dpath = os.path.join(self.inpath, self.stem)
        with open(dpath+'.evt','r') as evtf:
                self.evt_str = evtf.read()
        return

    def load_sample_data(self):
        dpath = os.path.join(self.inpath, self.stem)
        try:
            self.gzdat = np.load(dpath+'.gaz.npy')
            return
        except IOError: 
            pass
        try:
            self.gzdat = np.genfromtxt(dpath+'.gaz')
            np.save(dpath+'.gaz',self.gzdat)
        except IOError, e:
            print ('No gaze data found.')
            raise e
        return

    def sample_segments(self, gap=50):
        gaze_times = self.gzdat[:, 0]
        # edges are delimited by 'gap' timedifference
        ends = np.r_[np.where(np.diff(gaze_times) > gap)[0], gaze_times.size-1]
        starts = np.r_[ 0, np.where(np.diff(np.roll(gaze_times,1)) > gap)[0] ]
        edges_idx = np.array(zip(starts,ends))
        edges_times = np.array([gaze_times[i] for i in edges_idx])

        #### get sample rate and eye recorded from the evt str
        patt = re.compile( self._regex['srate_eye'] )
        srate_eye = re.findall(patt, self.evt_str)
        assert len(srate_eye) == edges_idx.shape[0]
        #screen resolution
        patt = re.compile( self._regex['resolution'] )
        coords = re.findall(patt, self.evt_str)
        assert len(coords) == edges_idx.shape[0]
        resols = [ (float(c[2])-float(c[0]), float(c[3])-float(c[1]) ) 
                        for c in coords]
        
        #### record these properties for every recording segment
        self.segments = [dict(
                    start_idx = prop[0][0], end_idx = prop[0][1],
                    start_tstamp= prop[1][0], end_tstamp=prop[1][1],
                    srate = int(prop[2][0]), eye= prop[2][1],
                    screen_x_pix = prop[3][0], screen_y_pix = prop[3][1],)
                for prop in izip(edges_idx, edges_times, srate_eye, resols) ]

        # colnames for gzdata; dependent on 'eye' value
        for sg in self.segments:
            sg['data_columns'] = _dcol_meanings[sg['eye']]
        return

    def add_behav_segments(self,fname=None):
        """
        add behavioral data to segment data:
        """
        if fname is None: fname = self.stem
        ipath = os.path.dirname(fname)
        if not ipath: ipath = '.'

        # constrtuct a ProcessBehavioralOutput obj.
        # and process the behavioral data
        self.behav = ProcessBehavioralOutput(ipath)
        self.behav.process_behav_file(fname)

        # add to the present data
        assert len(self.behav.behavioral_data) == len(self.segments)
        for i,dat in enumerate(self.behav.behavioral_data):
            self.segments[i].update(dat)
        return

    def eyelink_events(self):
        # compile all regex patterns
        patts = [re.compile(self._regex[k]) for k in  ['sacc','fix','blink'] ]
        # get lines; split into words:
        sacclines = [l.split() for l in re.findall(patts[0], self.evt_str)]
        fixlines = [l.split() for l in re.findall(patts[1], self.evt_str)]
        blinklines = [l.split() for l in re.findall(patts[2],self.evt_str)]

        ## sacc dicts:
        sacckeys = ['start_tstamp','end_tstamp','duration',
            'start_x','start_y','end_x','end_y', 'peak_velocity']
        saccdicts = [dict(zip(sacckeys, [float(word) for word in line[2:]] )) 
            for line in sacclines]
        for i,e in enumerate(sacclines): # line[1] has the eye-key
            saccdicts[i].update(eye=sacclines[i][1])
        ## fix dicts:
        fixkeys = ['start_tstamp','end_tstamp','duration', 'x','y','pupil_size']
        fixdicts = [dict(zip(fixkeys, [float(word) for word in line[2:]] ))
            for line in fixlines]
        for i,e in enumerate(fixlines):
            fixdicts[i].update(eye=fixlines[i][1])

        ## blink dicts:
        blinkkeys = ['start_tstamp','end_tstamp','duration']
        blinkdicts = [dict(zip(blinkkeys, [float(word) for word in line[2:]] ))
            for line in blinklines]
        for i,e in enumerate(blinklines):
            blinkdicts[i].update(eye=blinklines[i][1])
        
        self.saccades = saccdicts
        self.fixations = fixdicts
        self.blinks = blinkdicts
        return

    def interpolate_blinks(self, method = 'linear', lin_interpolation_points = [[-100],[100]], spline_interpolation_points = [[-0.15, -0.075],[0.075, 0.15]]):
        import scipy.interpolate as interpolate
        raw_pupil = self.gzdat[:, 3]
        interp_pup = raw_pupil.copy()
        srate = self.segments[0]['srate']
        blinks_marks = [(b['start_tstamp'], b['end_tstamp']) for b in self.blinks]
        timepoints = self.gzdat[:,0]
        # points_for_interpolation = np.array(np.array(spline_interpolation_points) * srate, dtype = int)
        # for bs,be in blinks_marks:
        #     samples = np.ravel(np.array([bs + points_for_interpolation[0], be + points_for_interpolation[1]]))
        #     theind = np.sum(np.array([timepoints == s for s in samples]), axis = 0).astype('bool')
        #     sample_indices = np.arange(raw_pupil.shape[0])[theind]
        #     try:
        #         spline = interpolate.InterpolatedUnivariateSpline(sample_indices, raw_pupil[sample_indices])
        #         interp_pup[sample_indices[1]:sample_indices[-2]] = spline(np.arange(sample_indices[1],sample_indices[-2]))
        #     except: # can not compute spline...
        #         try:
        #             interp_pup[sample_indices[1]:sample_indices[-2]] = np.nan
        #         except IndexError, e: # samples aren't even there:
        #             pass

        points_for_interpolation = np.array(np.array([-.1,.1]) * srate, dtype = int)
        for bs,be in blinks_marks:
            samples = np.ravel(np.array([bs + points_for_interpolation[0], be + points_for_interpolation[1]]))
            theind = np.sum(np.array([timepoints == s for s in samples]), axis = 0).astype('bool')
            sample_indices = np.arange(raw_pupil.shape[0])[theind]
            try:
                interp_pup[sample_indices[0]:sample_indices[1]] = np.linspace(interp_pup[sample_indices[0]], interp_pup[sample_indices[1]], sample_indices[1]-sample_indices[0])
            except:
                pass

        self.interp = interp_pup

        return

    def parse_custom_property(self,pattern, key='bla', overwrite=False):
        self.load_event_data()
        if not overwrite:
            assert not key in self.segments[0].keys()
        # find matches:
        matches = re.findall(re.compile(pattern), self.evt_str)
        assert len(matches) == len(self.segments)
        
        for i, match in enumerate(matches):
            try:
                self.segments[i][key]=float(match)
            except Exception, e:
                self.segments[i][key]=match
        return

    def custom_evt_parser(self, pattern, hook=None):
        # hook must be a void function, takes list of found matches, and 'self'
        if hook is None:
            hook = lambda x,a: x
        patt = re.compile(pattern)
        # load event data:
        self.load_event_data()
        # look for pattern in event data, and split into lines
        matches = re.findall(patt, self.evt_str)
        # call hook on pattern; hook may alter self
        hook(self, matches)
        return

    def run(self):
        self.load_sample_data()
        self.sample_segments()
        self.eyelink_events()
        if not self.beh_path is None:
            self.add_behav_segments(os.path.join(self.beh_path, self.stem))
        self.add_gzdat_segments()
        return

    def add_gzdat_segments(self):
        """
        Adds the gaze data of every recording segment to self.segments
        """
        assert self.segments
        assert self.gzdat is not None
        for segm in self.segments:
            gz_segm = self.gzdat[ segm['start_idx']:segm['end_idx'] ]
            segm['gz_data'] = gz_segm

if __name__ == '__main__':
    # ascp = ASCParser('ple17rb')
    # ascp.run()
    pass