#! /usr/bin/python

import os, re, argparse
import numpy as np
import tables as tb
import pandas as pd

from IPython import embed

import eyedata_table 
import eyedata_parser

# deal with layouts and other custom matters in parser


class Eye_IR_parser(eyedata_parser.ASCParser):
    """docstring for Eye_IR_parser"""
    def __init__(self, *args, **kwargs):
        super(Eye_IR_parser, self).__init__(*args, **kwargs)
        return


    def run(self):
        super(Eye_IR_parser, self).run()
        self.parse_custom_property(
            pattern='MSG\s(\d+)\sstart_trial', key='start_trial_tstamp')
        self.parse_custom_property(
            pattern='MSG\s(\d+)\sstop_trial', key='stop_trial_tstamp')
        self.parse_custom_property(
            pattern='MSG\s\d+\sstart_trial\s(\d+)', key='trial_index')
        return

###############################################################################

class Eye_IR_writer(eyedata_table.HDF_EyeDataWriter):
    """docstring for Eye_IR_writer"""
    def run_ascparser(self, asc_fname, behavioral_path):
        ap = self.ascparser=Eye_IR_parser(asc_fname, behavioral_path)
        ap.run()
        self.ascparser = ap
        self.stem = ap.stem

    def run(self, *args, **kwargs):
        self.run_ascparser(*args,**kwargs)
        self.store_event_data()
        self.store_gaze_data()
        return



###############################################################################
def edf2asc(inpath, outpath, infname='', batch_run=False):
    e2a = eyedata_parser.EDF2ASC(inpath, outpath)
    if batch_run:
        e2a.batch_run()
    else:
        e2a.create_asc(infname)
    return

def hdf(fname, inpath='', behavioral_path='behavdata', 
                h5fname='ple.h5', title=''):
    tabwriter = Eye_IR_writer(h5fname, title=title)
    tabwriter.run(os.path.join(inpath,fname), behavioral_path)
    return


def _CLI():
    parser = argparse.ArgumentParser()
    # TODO add arguments HERE! to call edf2asc or hdf()
    parser.add_argument('-E','--EDF2ASC', default=False, action = 'store_true',
        help='Run EDF2ASC if -E is given, runs parser otherwise')
    parser.add_argument('-i','--inpath', nargs='?', default='ascdata',
        help="inpath, of the edfs (with -E) or of the evt/gaz files", type=str)
    parser.add_argument('-o','--outpath',nargs='?', default='ascdata',
        help="(only for -E) where to put resulting asc-data", type=str)
    parser.add_argument('-f','--filename',nargs='+',
        help="file(s) to process with the parser", type=str)
    parser.add_argument('-t','--tabname',nargs='?', default = 'ple.h5',
        help="name of the table to save")
    parser.add_argument('-b','--behavioral_path',  nargs='?',
        default='behavdata', help="Path to behavioral expt output files")
    parser.add_argument('-B','--batch_run', default=False, action='store_true',
        help="(with -E) run EDF2ASC on all files in folder")
    
    return parser.parse_args()

def main():
    args = _CLI()
    if args.EDF2ASC:
        if args.batch_run:
            edf2asc(args.inpath,args.outpath,batch_run=True)
        else:
            for f in args.filename: edf2asc(args.inpath, args.outpath, f)
        return
    for f in args.filename:
        try:
            hdf(f, args.inpath, None,  # no behavdata
                    args.tabname, "LTM-priming_table")
        except tb.NodeError, e:
            print e
            print("{} already in {}; -- skipped".format(f,args.tabname))
            pass
    return

if __name__ == '__main__':
    # heye = Eye_IR_writer('ple.h5', title = 'LTM priming table')
    # heye.run('ascdata/ple17rb.evt', behavioral_path='behavdata')
    main()
    # call me through:
    # $ ./write -f `basename -a ascdata/*.evt` 